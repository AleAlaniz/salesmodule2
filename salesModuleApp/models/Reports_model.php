<?php 
Class Reports_model extends CI_Model
{
    public function GetSales($endDate,$campaigns)
    {
        $sqlCount = 'SELECT count(s.saleId) as count
        FROM sales s join users u
        ON (s.userId = u.userId)
        JOIN campaigns c
        ON (s.campaignId = c.campaignId)
        LEFT JOIN teamLeaders t
        ON u.teamLeaderId = t.teamLeaderId
        JOIN products p 
        ON (s.productId = p.productId)'; 
        

        $sql = 'SELECT s.saleId as "saleId", s.contractNumber, u.userName, CONCAT(u.name, " ", u.lastName) as "completeName", IFNULL(t.name, "No definido") as teamLeader, c.name as "campaignName",c.campaignId as "campaignId", p.name as "productName", s.comment, DATE_FORMAT(s.saleDate, "%d/%m/%Y %H:%i") as "saleDate", s.phone, s.dni, s.census as "census", s.commercial_offer as "commercial_offer", c.extra_data as "has_extra", c.extra_data_required as "has_extra_required"
        FROM sales s join users u
        ON (s.userId = u.userId)
        JOIN campaigns c
        ON (s.campaignId = c.campaignId)
        LEFT JOIN teamLeaders t
        ON u.teamLeaderId = t.teamLeaderId
        JOIN products p 
        ON (s.productId = p.productId)';

        //agrega las fechas a los parametros
        
        $params = array($this->input->post('start_date'),$endDate);
        
        //busca por las fechas enviadas por post
        $sql .= ' WHERE DATE(s.saleDate) >= str_to_date(?, "%d/%m/%Y") AND DATE(s.saleDate) <= str_to_date(?, "%d/%m/%Y")';
        $sqlCount .= ' WHERE DATE(s.saleDate) >= str_to_date(?, "%d/%m/%Y") AND DATE(s.saleDate) <= str_to_date(?, "%d/%m/%Y")';

        //verifica y busca por campaña especifica
        if($campaigns != 0)
        {
            $sql .= ' AND s.campaignId IN ?';
            $sqlCount .= ' AND s.campaignId IN ?';
            array_push($params, $this->input->post('campaigns'));
        }

        //dependiendo de los permisos del usuario, se mostraran las ventas propias o todas
        if($this->Identity_model->Validate('reports/viewonly') && !$this->Identity_model->Validate('reports/viewall')){
            $sql .= ' AND u.userId = ?';
            $sqlCount .= ' AND u.userId = ?';
            array_push($params, $this->session->UserId);
        }

        $sql .= ' ORDER BY s.saleDate DESC LIMIT ' .$_POST['start']. ',' .$_POST['length']. '   ';

        $dataquery = $this->db->query($sql, $params)->result();
        $countData = $this->db->query($sqlCount, $params)->row();

        $data = array();
        $cData = count($dataquery);
        
        for ($i=0; $i < $cData; $i++) { 
            $nestedData = array();

            $nestedData[] = $dataquery[$i]->saleId;
            $nestedData[] = $dataquery[$i]->campaignId;
            $nestedData[] = $dataquery[$i]->contractNumber;
            $nestedData[] = $dataquery[$i]->userName;
            $nestedData[] = $dataquery[$i]->completeName;
            $nestedData[] = $dataquery[$i]->teamLeader;
            $nestedData[] = $dataquery[$i]->campaignName;
            $nestedData[] = $dataquery[$i]->productName;
            $nestedData[] = $dataquery[$i]->comment;
            $nestedData[] = $dataquery[$i]->saleDate;
            $nestedData[] = $dataquery[$i]->dni;
            $nestedData[] = $dataquery[$i]->phone;
            $nestedData[] = $dataquery[$i]->census;
            $nestedData[] = $dataquery[$i]->commercial_offer;
            $nestedData[] = $dataquery[$i]->has_extra;
            $nestedData[] = $dataquery[$i]->has_extra_required;
            
            $data[] = $nestedData;
        }

        $json_data = array(
            "draw"              => intval($_POST['draw']),
            "recordsTotal"      => $countData->count,
            "recordsFiltered"   => $countData->count,
            "data"              => $data
        );
        echo json_encode($json_data); 
    }

    public function GetSalesExcel($campaigns=NULL)
    {
        $sql = 'SELECT s.saleId as "saleId", s.contractNumber, u.userName, CONCAT(u.name, " ", u.lastName) as "completeName", IFNULL(t.name, "No definido") as teamLeader, c.name as "campaignName",c.campaignId as "campaignId", p.name as "productName", s.comment, DATE_FORMAT(s.saleDate, "%d/%m/%Y %H:%i") as "saleDate", s.phone, s.dni, s.census, s.commercial_offer
        FROM sales s join users u
        ON (s.userId = u.userId)
        JOIN campaigns c
        ON (s.campaignId = c.campaignId)
        LEFT JOIN teamLeaders t
        ON u.teamLeaderId = t.teamLeaderId
        JOIN products p 
        ON (s.productId = p.productId)';

        //agrega las fechas a los parametros
        $params = array($this->input->post('start_date'),$this->input->post('end_date'));

        //busca las fechas enviadas por post
        $sql .= ' WHERE DATE(s.saleDate) >= str_to_date(?, "%d/%m/%Y") AND DATE(s.saleDate) <= str_to_date(?, "%d/%m/%Y")';

        if($this->input->post('campaign_id')){
            $sql .= ' AND s.campaignId IN ?';
            array_push($params, $this->input->post('campaign_id'));
        }

        //dependiendo de los permisos del usuario, se mostraran las ventas propias o todas
        if($this->Identity_model->Validate('reports/viewonly') && !$this->Identity_model->Validate('reports/viewall')){
            $sql .= ' AND u.userId = ?';
            $sqlCount .= ' AND u.userId = ?';
            array_push($params, $this->session->UserId);
        }

        $sql .= ' ORDER BY s.saleDate DESC ';
        
        return $this->db->query($sql, $params)->result();
    }
}
?>
