<?php 
Class Identity_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
    }
    
    public function Autenticate()
    {
        if($this->input->post('userName') && $this->input->post('password')){
            $sql = "SELECT userId, name, lastName, roleId, password FROM users WHERE userName = ? AND active = 1 LIMIT 1";
            $result = $this->db->query($sql,$this->input->post('userName'))->row();
            
            if(isset($result) && (crypt($this->input->post('password'),$result->password) == $result->password))
            {
               /* $sql = "SELECT name FROM rolePermissions rp
				JOIN permissions p
				ON (rp.permissionId = p.permissionId)
				WHERE rp.roleId = ?";

				$data = $this->db->query($sql,$result->roleId)->result();
				$permissions = array();
				foreach ($data as $permis) {
					$permissions [] = $permis->name;
				}
                */
                $sessionData = array(
                    'Loged'       => TRUE, 
                    'UserId'      => $result->userId,
                    'fullName'    => $result->name.' '.$result->lastName,
                    'RoleId'      => $result->roleId
                );

                $this->session->set_userdata($sessionData);
                $this->SetSelectedCampaign();
                echo 'success';
            }
            else
            {
                echo $this->lang->line("login_error_message");
            }
        }
        else{
            show_404();
        }
    }

    public function Validate($permissionName)
    {
        if ($this->session->has_userdata('Loged') && $this->session->has_userdata('UserId'))
        {
           $validateRole = FALSE;

           $sql = 'SELECT u.userId
           FROM (SELECT userId, roleId FROM users WHERE userId = ? AND active = 1) u JOIN roles r
           ON(u.roleId = r.roleId)
           JOIN rolePermissions rp
           ON(r.roleId = rp.roleId)
           JOIN permissions p
           ON(rp.permissionId = p.permissionId)
           WHERE p.name = ?';

           $validateRole = $this->db->query($sql, array($this->session->UserId, $permissionName))->row();
           return isset($validateRole);
       }
       else
       {
           return FALSE;
       }
   }

   public function getHierarchy($userId)
   {
        if($this->session->has_userdata('Loged') && $this->session->has_userdata('UserId')){

            $sql = "SELECT r.hierarchyId as hierarchyPosition
            FROM (
            SELECT roleId from users where userId = ? AND active = 1) user
            INNER JOIN roles r ON user.roleId = r.roleId";
            
            return $this->db->query($sql,$userId)->row()->hierarchyPosition;
        }
   }

   public function Logout()
   {
    $this->session->sess_destroy();
    header('Location:/'.FOLDERADD.'/');
}

    public function SetSelectedCampaign()
    {
        $sql = 'SELECT c.campaignId
             FROM campaigns c JOIN usersInCampaigns uc
             ON(c.campaignId = uc.campaignId)
             WHERE uc.userId = ? AND c.active = 1
             LIMIT 1';

             $campaign_result = $this->db->query($sql,  $this->session->UserId )->row();

             $this->session->selectedCampaign = (isset($campaign_result)) ? $campaign_result->campaignId : null;
    }
}