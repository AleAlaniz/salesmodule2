<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sales_model extends CI_Model {

    
    public function __construct()
    {
        parent::__construct();
    }
    
    public function Create($sale)
    {
        if(isset($sale))
        {
            $this->db->insert('sales', $sale);
            return 'success';
        }
        return 'error';
    }
    
    public function Delete($saleId)
    {
        $this->db->where('saleId', $saleId);
        $this->db->delete('sales');
        return "success";
    }

    public function GetSaleById($saleId)
    {
        $sql = "SELECT * FROM sales WHERE saleId = ? LIMIT 1";
        $res = $this->db->query($sql,array($saleId))->row();
        return $res;
    }

    public function edit($sale)
    {
        $saleId = $sale['saleId'];

        $origDate = $sale['date'];
        $date = str_replace('/', '-', $origDate);
        $newDate = date("Y-m-d", strtotime($date));

        $sale_date = $newDate.' '. $sale['hour'];

        $updateArray = array(
            'productId'      => $sale['product'],
            'saleDate'       => $sale_date,
            'contractNumber' => $sale['contractNumber'],
            'comment'        => $sale['comment'],
            'phone'          => $sale['phone'],
            'dni'            => $sale['dni'],
            'census'            => $sale['census'],
            'commercial_offer'  => $sale['commercial_offer'],
        );
        $this->db->where('saleId',$saleId);
        $affected_rows = $this->db->update('sales', $updateArray);

       return ($affected_rows == 1)? true : false;
    }

}

/* End of file Sales_model.php */
