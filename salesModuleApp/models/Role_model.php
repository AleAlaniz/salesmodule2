<?php 
Class Role_model extends CI_Model
{
    
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
    }

    public function getRoles()
    {
        $hierarchyId = $this->Identity_model->getHierarchy($this->session->UserId);
        $sql = "SELECT * FROM roles WHERE hierarchyId > ? OR roleId = ?";
    	return $this->db->query($sql,array($hierarchyId,$this->session->RoleId))->result();
    }

    public function getRole($roleId)
    {

    	$res = new StdClass();
    	$sql = 
    	"SELECT roles.roleId,roles.name, roles.hierarchyId
		FROM roles 
        WHERE roles.roleId = ?
        LIMIT 1";
		$res = $this->db->query($sql,$roleId)->row();

        if(isset($res)){

            $sql = 
            "SELECT permissions.permissionId,permissions.description,rolePermissions.rolePermissionId
            FROM permissions
            LEFT JOIN rolePermissions ON permissions.permissionId = rolePermissions.permissionId
            AND rolePermissions.roleId = ?
            ORDER BY permissions.description asc";
            $res->rolePermissions = $this->db->query($sql,$roleId)->result();
    
            $sql = 
            "SELECT COUNT(rolePermissions.rolePermissionId) as sizeOfPermissions
            FROM permissions
            LEFT JOIN rolePermissions ON permissions.permissionId = rolePermissions.permissionId
            AND rolePermissions.roleId = ?
            AND rolePermissions.rolePermissionId IS NOT NULL";
            $res->sizeOfPermissions = $this->db->query($sql,$roleId)->row()->sizeOfPermissions;
            
        }
        
    	return $res;
    }

    public function getPermissions()
    {
    	return $this->db->query("SELECT * FROM permissions ORDER BY description asc")->result();
    }

    public function createRole($form){

    	$this->db->insert('roles',array('name' => htmlspecialchars($form['name']),'hierarchyId' => $form['hierarchy']));

    	$insert_id   = $this->db->insert_id();
    	$permissions = $this->getPermissions();

    	if (isset($form['active'])){
    
    		foreach ($form['active'] as $active) {

    			$this->db->insert('rolePermissions',array('roleId' => $insert_id, 'permissionId' => $active));
    		}
    	}	
    }

    public function editRole($form){

    	$sql = "UPDATE roles
    	SET name = ?,
        hierarchyId = ?
    	WHERE roles.roleId = ?";
    	$this->db->query($sql,array('name' => htmlspecialchars($form['name']),'hierarchyId' => $form['hierarchy'],"roleId" =>$form['roleId']));

    	$sql = "DELETE FROM rolePermissions
    	WHERE rolePermissions.roleId = ?";
    	$this->db->query($sql,array("roleId" =>$form['roleId'] ));

    	if (isset($form['active'])){

    		foreach ($form['active'] as $active) {

    			$this->db->insert('rolePermissions',array('roleId' => $form['roleId'], 'permissionId' => $active));
    		}
    	}	
    } 

    public function deleteRole($form){

    	$sql = "DELETE FROM rolePermissions
    	WHERE rolePermissions.roleId = ?";
    	$this->db->query($sql,array("roleId" =>$form['roleId'] ));

    	$sql = "DELETE FROM roles
    	WHERE roles.roleId = ?";
    	$this->db->query($sql,array("roleId" =>$form['roleId'] ));
    } 

    public function roleIdExists($roleId)
    {
        $sql = "SELECT r.roleId
        FROM roles r
        WHERE r.roleId = ?
        LIMIT 1";
        $query = $this->db->query($sql,$roleId)->row();

        return isset($query);
    }
}

?>