<table id="data_table" class="table table-hover">
    <thead>
        <tr>
            <th><?php echo ($this->lang->line('report_contract_number')) ?></th>
            <th><?php echo ($this->lang->line('report_username')) ?></th>
            <th><?php echo ($this->lang->line('report_agent')) ?></th>
            <th><?php echo ($this->lang->line('report_team_leader')) ?></th>
            <th><?php echo ($this->lang->line('report_campaign')) ?></th>
            <th><?php echo ($this->lang->line('report_product')) ?></th>
            <th><?php echo ($this->lang->line('report_comment')) ?></th>
            <th><?php echo ($this->lang->line('report_sale_date')) ?></th>
            <th><?php echo ($this->lang->line('report_dni')) ?></th>
            <th><?php echo ($this->lang->line('report_phone')) ?></th>
            <?php if ($this->Identity_model->Validate('sales/edit') || $this->Identity_model->Validate('sales/delete')) { ?>
                <th><?php echo ($this->lang->line('actions')) ?></th>
            <?php } ?>
        </tr>
    </thead>
</table>
<script type="text/javascript">
    $(document).ready(function() {
            var dataTable = $('#data_table').DataTable( {
                "processing": true,
                "serverSide": true,
                "bFilter": false,
                "ajax":{
                    url :"getsales", // json datasource
                    type: "post",  // method  , by default get
                    error: function(){  // error handling
                        $(".employee-grid-error").html("");
                        $("#employee-grid").append('<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
                        $("#employee-grid_processing").css("display","none");
                        
                    }
                },
                language: {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ ventas",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando ventas del _START_ al _END_ de un total de _TOTAL_ ventas",
                    "sInfoEmpty":      "Mostrando ventas del 0 al 0 de un total de 0 ventas",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ ventas)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                },
                "columnDefs": [ {
                  "targets": 10,
                   defaultContent: '<div class="btn-group" role="group" aria-label="Basic example"><?php if($this->Identity_model->Validate('sales/edit')) {?><button class="btn btn-warning" title="<?php echo $this->lang->line('sale_edit'); ?>" data-toggle="modal" data-target="#edit-sale" name="PHP_ECHO_KEY" id="sale_edit_button"><i class="fas fa-edit"></i></button><?php } ?><?php if($this->Identity_model->Validate('sales/delete')) {?><button class="btn btn-danger deleteSale" id="ECHO_ITEM_SALEID" title="<?php echo $this->lang->line('sale_delete'); ?>"><i class="fas fa-trash"></i></button><?php } ?></div>'
                } ]
            } );
        } );
</script>
