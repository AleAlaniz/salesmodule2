		<?php $dataCount = 0 ?>
		<div id="top" class="card">
			<div class="card-header">
				<h3><?php echo($this->lang->line('general_reports')) ?></h3>
			</div>
			<div class="card-body">

				<?php if(isset($_SESSION['saleMessage'])){ ?>
					<div class="alert alert-info alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<strong><i class="fas fa-check"></i></strong> 
						<?php if ($_SESSION['saleMessage'] == 'delete'){
							echo $this->lang->line('report_sales_deletemessage');
						}
						else{
							if ($_SESSION['saleMessage'] == 'edit') {
								echo $this->lang->line('report_sales_editmessage');
							}
						}

						?>
					</div>		
				<?php } ?>
				<form method="post" action="" class="form-line">
					<div class="row mb-4">
						<div class="col-sm-5">
							<div class="form-group">
								<label for="campaign_id">Campaña</label>
								<select name="campaign_id[]" class="form-control" multiple id="campaign_id">
									<?php
									$campaignsCount = count($campaigns);
									for($i = 0; $i < $campaignsCount; $i++)
									{
										echo('<option value="'.$campaigns[$i]->campaignId.'" '.set_select('campaign_id', $campaigns[$i]->campaignId, ($this->input->post('campaign_id') == $campaigns[$i]->campaignId)).' >'.$campaigns[$i]->name.'</option>');
									}
									?>
								</select>
							</div>
							<?php echo form_error('campaign_id'); ?>

						</div>
						<div class="col-sm-5">
							<div class="form-group">
								<label for="start_date">Rango de fechas</label>
								<div class="input-group input-daterange" id="range_date">
									<input type="text" class="form-control" name="start_date" value="<?php echo(set_value('start_date', date('d/m/Y'),TRUE));?>" id="start_date">
									<div class="input-group-prepend input-group-append">
										<i class="input-group-text fas fa-calendar-alt fa-lg"></i>
									</div>
									<input type="text" class="form-control" name="end_date" id="end_date" value="<?php echo(set_value('end_date', date('d/m/Y'),TRUE));?>">
								</div>
							</div>
							<?php echo form_error('end_date'); ?>
						</div>
						<div class="col-sm-2">
							<button type="button" id="search" class="form-control btn btn-sm btn-outline-info" style="margin-top:31px">Buscar</button>
						</div>
					</div>
					<div class="row mb-3">
						<div class="col-sm-6">
							<h4>
								<?php echo($this->lang->line('report_sales'));
								if($dataCount > 0)
									echo('&nbsp;<span class="badge badge-success">'.$dataCount.'</span>');
								?>
							</h4>
						</div>
						<div class="col-sm-6">
							<?php
							if($this->Identity_model->Validate('reports/viewall')){ ?>
								<button class="btn btn-outline-success float-right" name="export_sales_report" type="submit" title="Exportar a Excel"><i class="fas fa-file-excel"></i> <?php echo($this->lang->line('report_export'));?></button>
							<?php } ?>
						</div>

					</div>
				</form>

				<div class="modal fade" id="popDelete" tabindex="-1" role="dialog">
					<div class="modal-dialog modal-sm">
						<div class="modal-content">
							<div class="modal-body">
								<p><?php echo $this->lang->line('report_sale_areyousure_delete')?>
								<div class="row">
									<div class="col-md-12 text-center">
										<span class="btn btn-success" id="acceptDelete"><?php echo $this->lang->line('general_accept'); ?></span>
										<span id="loading" class="d-none"><i class="fas fa-spinner fa-spin fa-2x"></i></span>
										<span id="cancel" class="btn btn-danger" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_cancel'); ?></span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-12">
						<div class="table-responsive">
							<table id="data_table" class="table table-hover">
								<thead>
									<tr>
										<th><?php echo ($this->lang->line('sale_id')) ?></th>
										<th><?php echo ($this->lang->line('campaign_id')) ?></th>
										<th><?php echo ($this->lang->line('report_contract_number')) ?></th>
										<th><?php echo ($this->lang->line('report_username')) ?></th>
										<th><?php echo ($this->lang->line('report_agent')) ?></th>
										<th><?php echo ($this->lang->line('report_team_leader')) ?></th>
										<th><?php echo ($this->lang->line('report_campaign')) ?></th>
										<th><?php echo ($this->lang->line('report_product')) ?></th>
										<th><?php echo ($this->lang->line('report_comment')) ?></th>
										<th><?php echo ($this->lang->line('report_sale_date')) ?></th>
										<th><?php echo ($this->lang->line('report_dni')) ?></th>
										<th><?php echo ($this->lang->line('report_phone')) ?></th>
										<th><?php echo ($this->lang->line('census_id')) ?></th>
										<th><?php echo ($this->lang->line('commercial_offer')) ?></th>
										<th class="d-none"></th>
										<th class="d-none"></th>
										<th><?php echo ($this->lang->line('actions')) ?></th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>

			</div>
		</div>
		<span style="display:none; position: fixed;bottom: 50px; right: 55px; z-index:999; opacity:0.6"><i id="page_up" class="cat-pointer text-info fas fa-chevron-circle-up fa-3x"></i></span>

		<!-- editSaleModal -->
		<div class="modal fade" id="edit-sale" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel"><?php echo $this->lang->line('sale_edit'); ?></h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<form id="edit-sale-form" action="sales/editSale" method="POST" novalidate>
							<div class="form-group row">
								<label for="contractNumber" class="col-md-2"><span class="font-weight-bold"><?php echo $this->lang->line('sales_contractNumber');?></span><span class="text-danger"><strong> *</strong></span></label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="contractNumber" name="contractNumber" placeholder="<?php echo $this->lang->line('sales_contractNumber');?>">
								</div>
							</div>
							<div class="form-group row">
								<label for="campaign" class="col-md-2"><span class="font-weight-bold"><?php echo $this->lang->line('sales_campaign');?></span></label>
								<div class="col-md-10">
									<label for="campaign" id="campaign"></label>
								</div>
							</div>
							<div class="form-group row">
								<label for="product" class="col-md-2"><span class="font-weight-bold"><?php echo $this->lang->line('main_product');?></span><span class="text-danger"><strong> *</strong></span></label>
								<div class="col-md-10">
									<p class="text-danger d-none" id="emptyCampaign"><?php echo $this->lang->line('product_empty_campaign'); ?></p>
									<select name="product" id="product" name="product" class="form-control">
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="comment" class="col-md-2"><span class="font-weight-bold"><?php echo $this->lang->line('sales_comment');?></span></label>
								<div class="col-md-10">
									<textarea maxlength="500" class="form-control" id="comment" name="comment" value="<?php echo set_value('name',$this->input->post('comment'));?>"></textarea>
									<p><?php echo $this->lang->line('max_length_text');?></p>
								</div>
							</div>
							<div class="form-group row">
								<label for="phone" class="col-md-2 pr-3"><span class="font-weight-bold"><?php echo $this->lang->line('sales_phone');?></span><span class="text-danger"><strong> *</strong></span></label>
								<div class="col-md-10">
									<input type="text" class="form-control" id="phone" name="phone" autocomplete="off">
								</div>
							</div>

							<div class="form-group row">
								<label for="dni" class="col-md-2"><span class="font-weight-bold"><?php echo $this->lang->line('sales_dni');?></span><span class="text-danger"><strong> *</strong></span></label>
								<div class="col-md-10">
									<input type="text" class="form-control" id="dni" name="dni" autocomplete="off">
								</div>
							</div>

							<div class="form-group row">
								<label for="date" class="col-md-2"><span class="font-weight-bold"><?php echo $this->lang->line('report_sale_time_date');?></span><span class="text-danger"><strong> *</strong></span></label>
								<div class="col-md-10">
									<input type="text" class="form-control" name="date" id="date">
									<input type="time" class="form-control" name="hour" id="hour" step="1">
								</div>
							</div>

							<div class="form-group row" id="extra_data_census_input">
								<label for="census" class="col-md-2"><span class="font-weight-bold"><?php echo $this->lang->line('sales_census');?></span>
									<span class="text-danger" id="extra_data_census_input_obligatory"><strong> *</strong></span>
								</label>
								<div class="col-md-10">
									<input type="text" class="form-control" id="census" name="census" autocomplete="off" value="<?php echo set_value('name',$this->input->post('census'));?>">
								</div>
							</div>
							<div class="form-group row" id="extra_data_commercial_offer_input">
								<label for="commercial_offer" class="col-md-2"><span class="font-weight-bold"><?php echo $this->lang->line('sales_commercial_offer');?></span>
									<span class="text-danger" id="extra_data_commercial_offer_input_obligatory"><strong> *</strong></span>
								</label>
								<div class="col-md-10">
									<select class="form-control" id="commercial_offer" name="commercial_offer">
										<option></option>
										<option>Nivel 1</option>
										<option>Nivel 2</option>
										<option>Nivel 3</option>
									</select>
								</div>
							</div>
							<div class="form-group row d-none">
								<input type="text" class="form-control" name="saleId" id="saleId" value="">
								<input type="text" class="form-control" name="campaignId" id="campaignId" value="">
							</div>

							<div class="form-group row form-error">

							</div>		

							<div class="modal-footer">
								<button type="submit" class="btn btn-success"><?php echo $this->lang->line('general_edit');?></button>
								<button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo $this->lang->line('general_cancel');?></button>
							</div>

						</form>
					</div>
				</div>
			</div>
		</div>

		<link href="<?php echo(asset_url()) ?>libraries/multiselect/jquery.multiselect.css" rel="stylesheet" />
		<script src="<?php echo(asset_url()) ?>libraries/multiselect/jquery.multiselect.js"></script>

		<script type="text/javascript">
			$(function(e){

				$('#extra_data_census_input').hide();
				$('#extra_data_commercial_offer_input').hide();
				$('#extra_data_census_input_obligatory').hide();
				$('#extra_data_commercial_offer_input_obligatory').hide();
				

				

				var dataTable = $('#data_table').DataTable( {
					"processing": true,
					"serverSide": true,
					"bFilter": false,
					"ordering": false,
					"lengthMenu": [ 10, 25, 50, 100, 250, 500 ],
					"ajax":{
						url :"reports/getsales", // json datasource
						data: function(d){
							d.start_date	= $('#start_date').val(),
							d.end_date		= $('#end_date').val(),
							d.campaigns		= $('#campaign_id').val().length > 0 ? $('#campaign_id').val() : 0
						},
						type: "post",  // method  , by default get
						error: function(){  // error handling
							$(".employee-grid-error").html("");
							$("#employee-grid").append('<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
							$("#employee-grid_processing").css("display","none");
						}
					},
					language: {
						"sProcessing":     "Procesando...",
						"sLengthMenu":     "Mostrar _MENU_ ventas",
						"sZeroRecords":    "No se encontraron resultados",
						"sEmptyTable":     "Ningún dato disponible en esta tabla",
						"sInfo":           "Mostrando ventas del _START_ al _END_ de un total de _TOTAL_ ventas",
						"sInfoEmpty":      "Mostrando ventas del 0 al 0 de un total de 0 ventas",
						"sInfoFiltered":   "(filtrado de un total de _MAX_ ventas)",
						"sInfoPostFix":    "",
						"sSearch":         "Buscar:",
						"sUrl":            "",
						"sInfoThousands":  ",",
						"sLoadingRecords": "Cargando...",
						"oPaginate": {
							"sFirst":    "Primero",
							"sLast":     "Último",
							"sNext":     "Siguiente",
							"sPrevious": "Anterior"
						},
						"oAria": {
							"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
							"sSortDescending": ": Activar para ordenar la columna de manera descendente"
						}
					},
					"columnDefs": 
					[ 
					{
						"targets": 0,
						"className": "d-none"
					},
					{
						"targets": 1,
						"className": "d-none"
					},{
						"targets": 14,
						"className": "dissapear"
					},{
						"targets": 15,
						"className": "dissapear"
					},
					
					{
						"targets": 16,
						defaultContent: '<div class="btn-group" role="group" aria-label="Basic example"><?php if($this->Identity_model->Validate('sales/edit')) {?><button class="btn btn-warning sale_edit_button" title="<?php echo $this->lang->line('sale_edit'); ?>" data-toggle="modal" data-target="#edit-sale"><i class="fas fa-edit"></i></button><?php } ?><?php if($this->Identity_model->Validate('sales/delete')) {?><button class="btn btn-danger deleteSale" title="<?php echo $this->lang->line('sale_delete'); ?>"><i class="fas fa-trash"></i></button><?php } ?></div>'
					}
					]
				});
				$('#search').click(function () {
					dataTable.ajax.reload();
				});

				$('#campaign_id').multiselect({
					columns: 1,
					placeholder: 'Selecciona la/s campañas',
					search: true
				});

				var dataEdit = "", saleId = "", campaignId = "";
				$('#data_table').on('click' ,'.sale_edit_button',function (e) {
					dataEdit = $(this).closest('tr')[0].innerText.split('	');
					console.log(dataEdit);
					saleId = $(this).closest('tr')[0].cells[0].innerText;
					campaignId = $(this).closest('tr')[0].cells[1].innerText;
				})

				$( "#edit-sale" ).on('show.bs.modal', function(e){
					$('#saleId').val(saleId);
					$('#campaignId').val(campaignId);
					$('#contractNumber').val(dataEdit[0]);
					$('#campaign').text(dataEdit[4]);
			$('#comment').val(dataEdit[6]);//
			$('#date').datepicker('setDate', dataEdit[7].split(' ')[0]);
			$('#dni').val(dataEdit[8]);
			$('#phone').val(dataEdit[9]);
			saleHour = dataEdit[7].split(' ')[1];
			$('#hour').val(saleHour);

			if(dataEdit[12] == "1"){
				console.log('AJAAAAAAAAAAAA');
				$('#extra_data_census_input').show();
				$('#extra_data_commercial_offer_input').show();
				$('#census').val(dataEdit[10]);
				$('#commercial_offer').val(dataEdit[11]);
				
				
			}

			if(dataEdit[13] == "1"){
				
				$('#extra_data_census_input_obligatory').show();
				$('#extra_data_commercial_offer_input_obligatory').show();
			}

			



			$.post({
				url: 'campaigns/getProducts',
				type: 'post',
				data: {campaignId : campaignId},
				dataType:'json',
				success: function( data){
					var $productSelect = $("#product");
					$productSelect.empty();
					for (var i = 0; i < data.length; i++) {
						$productSelect.append($("<option />").val(data[i].productId).text(data[i].name));
					}
				},
				error: function( jqXhr, textStatus, errorThrown ){
					console.log( errorThrown );
				}
			});
			
		});

				$("#edit-sale-form").submit(function(e){
					e.preventDefault();
					$('.form-error').empty();

					$.ajax({
						type: "POST",
						data:{'form': $("#edit-sale-form").serialize()},
						url: 'sales/editSale',
						dataType: "json",
						success:function(data)
						{
							if (data.status =='form-error'){
								$('.form-error').append('<div class="text-danger col-sm-10">'+data.message+'</div>');
							}
							else{
								if (data.status == 'success') {

									$( "#edit-sale" ).modal('toggle');
									window.location.href = window.location.href;
								}
								else{
									console.log('error!');
								}
							}
						},
						error:function()
						{
							console.log('Error');
						}
					});
				});

				$('#range_date').datepicker(
				{
					language 		: 'es',
					autoclose 		: true,
					todayHighlight 	: true,
					todayBtn 		: "linked"
				});

				$('#date').datepicker({
					language 		: 'es',
					autoclose 		: true,
					todayHighlight 	: true,
					todayBtn 		: "linked"
				});

		//fix para evitar la propagacion del evento cuando se presiona el datepicker dentro del modal
		$("#date").datepicker().on('show.bs.modal', function(event) {
			event.stopPropagation(); 
		});


		$(window).scroll(function()
		{
			var height = $(window).scrollTop();
			if (height > 100) {
				$("#page_up").parent().fadeIn();
			} else {
				$("#page_up").parent().fadeOut();
			}
		});

		$("#page_up").on('click', function()
		{
			$(document).scrollTop( $("#top").offset().top );
		})

		var saleId;
		$('#data_table').on('click' ,'.deleteSale',function (e) {
			saleId = $(this).closest('tr')[0].cells[0].innerText;
			$('#popDelete').modal('show');
		})


		$('#acceptDelete').on('click',function(){
			$('#acceptDelete').addClass('d-none');
			$('#cancel').addClass('d-none');
			$('#loading').removeClass('d-none');
			var url = '/<?php  echo FOLDERADD; ?>/sales/delete' ;
			$.ajax({
				method 	: 'POST',
				url 	: url,
				data 	: {'saleId' : saleId}
			}).done(function(data) {
				if(data == "success"){
					location.reload();
				}
			});
		})

		$('#edit-sale').on('hidden.bs.modal', function (e) {
			$('#extra_data_census_input').hide();
			$('#extra_data_commercial_offer_input').hide();
			$('#extra_data_census_input_obligatory').hide();
			$('#extra_data_commercial_offer_input_obligatory').hide();
		})
	});
</script>

