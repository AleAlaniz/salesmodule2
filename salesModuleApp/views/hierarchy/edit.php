<?php $this->load->view('_shared/_admin_nav.php') ?>
<div class="card">
	<div class="card-header">
		<h3><?php echo $this->lang->line('hierarchy_edit');?></h3>
	</div>
	<div class="card-body">
		<form method="POST" novalidate action="/<?php echo FOLDERADD; ?>/hierarchy/editHierarchy/<?php echo $currentHierarchy->hierarchyId; ?>">
			<div class="form-group row">
				<label class="col-md-2" for="name"><span class="font-weight-bold"><?php echo $this->lang->line('general_name');?>:</span><span class="text-danger"><strong> *</strong></span></label>
				<div class="col-md-10">
					<input type="text" class="form-control" id="name" name="name" placeholder="<?php echo $this->lang->line('general_name');?>"  value="<?php echo  $currentHierarchy->name ?>" maxlength="50">
					<p class="text-danger"><?php echo form_error('name'); ?></p>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-md-2" for="name">
					<span class="font-weight-bold">
					</span>
					<span class="text-danger">
					</span>
				</label>
				<div class="col-md-10">
					<div class="btn-group" role="group" aria-label="Basic example">
						<?php if (!$topHierarchy && $higherLevel) { ?>
							<button type="button" id="first" class="btn btn-info active" onclick="sameLevel(<?php echo $currentHierarchy->hierarchyId ?>)">
								<?php echo $this->lang->line('admin_hierarchy_same_level') ?>
							</button>
							<button type="button" id="not_first" class="btn btn-info">
								<?php echo $this->lang->line('admin_hierarchy_change_level') ?>
							</button>
							<select id="hierarchyPositions" class="form-control" name="hierarchy" id="hierarchy">
								<?php foreach ($hierarchys as $hierarchy) { ?>
									
									<option value="<?php echo $hierarchy->hierarchyId ?>" <?php echo set_select('hierarchy',$hierarchy->hierarchyId,($hierarchy->hierarchyId == $this->input->post('newHierarchy')))?>>
										<?php echo $hierarchy->name ?>
									</option>
								<?php   } ?>
							</select>
						<?php } else{ ?>
							<div class="alert alert-danger">
        					   	<?php echo $this->lang->line('admin_hierarchy_no_permission'); ?>
        					</div>
						<?php } ?>
					</div>
					
				</div>			
				<p class="text-danger"><?php echo form_error('newHierarchy'); ?></p>
			</div>
			<input type="text" class="d-none" name="originalHierarchyId" value="<?php echo $currentHierarchy->hierarchyId ?>">
			<input type="radio" class="d-none" name="isSameLevel" checked>
			<div class="row">
				<div class="col-md-12 text-center">
					<button type="submit" class="btn btn-success"><?php echo $this->lang->line('general_save');?></button>
					<a href="/<?php echo FOLDERADD; ?>/hierarchy" class="btn btn-danger"><?php echo $this->lang->line('general_cancel');?></a>
				</div>
			</div>
		</form>
	</div>

</div>
</div>
<script type="text/javascript">

	function sameLevel(hierarchyId) {

		$('[name="isSameLevel"]').prop('checked',true);
		$('#hierarchyPositions').hide();
		$('#first').addClass('active');
		$('#not_first').removeClass('active');
	} 

	$(function() {

		$('#not_first').on('click', function () {
			$('[name="isSameLevel"]').prop('checked',false);
			$('#hierarchyPositions').show();
			$('#first').removeClass('active');
			$('#not_first').addClass('active');
		})

		$('#edit').addClass('active');
		$('#hierarchyPositions').hide();

		if ($('#not_first').hasClass('active')){
			$('#hierarchyPositions').show();
		}

		if ($('#first').hasClass('active')){
			$('#hierarchyPositions').hide();
		}
	})
</script>
