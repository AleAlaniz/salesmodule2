	<?php $this->load->view('_shared/_admin_nav.php');
	if($this->session->flashdata('saveResult')){ ?>

		<div class="alert alert-info alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong><i class="fas fa-check"></i></strong>
			<?php echo($this->session->flashdata('saveResult')); ?>
		</div>
	<?php } ?> 
	<div class="card">
		<div class="card-header">
			<h3>
				<?php echo($this->lang->line('campaign_details')); ?>
				<button class="btn btn-danger float-right" data-toggle="modal" data-target="#delete-modal">
					<?php echo($this->lang->line('general_delete')); ?>
				</button>
			</h3>
			
		</div>

		<div class="card-body">
			<?php if(isset($campaign_data['campaign'])) 
			{?>
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<dl>
								<dt><?php echo($this->lang->line('campaign_name').':'); ?></dt>
								<dd><?php echo($campaign_data['campaign']->name); ?></dd>
							</dl>
						</div>
						<div class="col-md-6">
							<dl>
								<dt><?php echo($this->lang->line('campaign_description').':'); ?></dt>
								<dd><?php echo($campaign_data['campaign']->description); ?></dd>
							</dl>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<dl>
								<dt><?php echo($this->lang->line('main_users').':'); ?></dt>
								<dd>
									<div>
										<?php
										$usersCount = count($campaign_data['users']);

										if($usersCount == 0)
										{
											echo('<p>'.$this->lang->line('users_empty').'</p>');
										}
										else
										{
											$permissionEnabled = $this->Identity_model->Validate('users/admin');
											for($i = 0; $i < $usersCount; $i++)
											{
												if($permissionEnabled)
													echo('<a href="/'.FOLDERADD.'/users/view/'.$campaign_data['users'][$i]->userId.'" class="badge badge-info" style="margin-right:5px" target="_blank">'.$campaign_data['users'][$i]->completeName.' </a>');
												else
													echo('<span class="badge badge-info" style="margin-right:5px">'.$campaign_data['users'][$i]->completeName.' </span>');
											}
										}

										?>
									</div>
								</dd>
							</dl>
						</div>
						<div class="col-md-6">
							<dl>
								<dt><?php echo($this->lang->line('campaign_products').':'); ?></dt>
								<dd>
									
									<?php

									$productsCount = count($campaign_data['products']);

									if($productsCount == 0)
									{
										echo('<p>'.$this->lang->line('products_empty').'</p>');
									}
									else
									{
										foreach ($campaign_data['products'] as $key => $product) { ?>
											<a class="badge badge-info" id="<?php echo $key; ?>" data-toggle="collapse" href="" name="productView">
												<?php echo $product->name; ?>
											</a>
											<?php 
										}
									}?>
									<div class="collapse" id="productDetail">
										
									</div>
								</dd>
							</dl>
						</div>
					</div>

				</div>
				<?php
			} ?>
		</div>
	</div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="delete-modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><?php echo($this->lang->line('campaign_del')); ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body alert alert-danger">
			<strong><i class="fas fa-skull"></i></strong>
			<?php echo($this->lang->line('campaign_delete_alert')); ?>
		</div>
      <div class="modal-footer">
        <form method="post" action="/<?php echo FOLDERADD; ?>/Campaigns/delete/<?php echo(set_value('campaign_id', $campaign_data['campaign']->campaignId)) ?>">
			<input type="hidden" name="campaign_id" id="campaign_id" value="<?php echo(set_value('campaign_id', $campaign_data['campaign']->campaignId)) ?>" />
			<div class="text-center">
				<button type="submit" class="btn btn-success"><?php echo($this->lang->line('general_delete')) ?></button>
				<button class="btn btn-danger" data-dismiss="modal" aria-label="Close"><?php echo($this->lang->line('general_cancel')) ?></button>
				<?php echo(form_error('campaign_id'))?>
			</div>
		</form>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
	$(function() {
		$('#view').addClass('active');
		$('#delete-modal').on('shown.bs.modal', function () {
	  		$('#delete-modal').trigger('focus')
		})
		var products = <?php echo json_encode($campaign_data['products']); ?>;
		
		$('[name="productView"]').on('click',showProductDetails)
		function showProductDetails() {

			$('#productDetail').hide();
			$('#productDetail').empty();
			
			var description = products[parseInt($(this).attr('id'))].detail;
			$('#productDetail').append('<span>'+description+'</span>');
			$('#productDetail').show('slow');
		}
	})
</script>