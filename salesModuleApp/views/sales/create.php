    <div class="card">
        <div class="card-header">
            <h3><?php echo $this->lang->line('sales_create');?></h3>
        </div>
        <div class="card-body">
            <?php if($this->Identity_model->Validate('sales/admin')){ ?>
                <div class="alert alert-success alert-dismissible d-none" role="alert" id="saleMessage">
                    <strong><i class="fas fa-check"></i></strong> 
                    <?php echo $this->lang->line('sales_createmessage'); ?>
                    <p><?php echo $this->lang->line('sales_want_to_resale');?></p>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <button type="button" class="btn btn-success" onclick="emptyForm(true)"><?php echo $this->lang->line('general_yes');?></button>
                            <button type="button" class="btn btn-danger" onclick="emptyForm()"><?php echo $this->lang->line('general_no');?></button>
                        </div>
                    </div>
                </div>
                <form id="form">

                    <div class="form-group row">
                        <label for="contractNumber" class="col-md-2"><span class="font-weight-bold"><?php echo $this->lang->line('sales_contractNumber');?></span><span class="text-danger"><strong> *</strong></span></label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="contractNumber" name="contractNumber" autocomplete="off" value="<?php echo set_value('name',$this->input->post('contractNumber'));?>">
                        </div>
                    </div>

                <div class="form-group row">
                    <label for="product" class="col-md-2"><span class="font-weight-bold"><?php echo $this->lang->line('main_product');?></span><span class="text-danger"><strong> *</strong></span></label>
                    <div class="col-md-10">
                        <p class="text-danger d-none" id="emptyCampaign"><?php echo $this->lang->line('product_empty_campaign'); ?></p>
                        <select name="product" id="product" name="product" class="form-control">
                           <?php 
                           foreach ($data->products as $product) { ?>
                            <option value="<?php echo $product->productId ?>" ><?php echo  $product->name ?></option>
                        <?php   } ?>
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label for="comment" class="col-md-2"><span class="font-weight-bold"><?php echo $this->lang->line('sales_comment');?></span></label>
                <div class="col-md-10">
                    <textarea maxlength="500" class="form-control" id="comment" name="comment" value="<?php echo set_value('name',$this->input->post('comment'));?>"></textarea>
                    <p><?php echo $this->lang->line('max_length_text');?></p>
                </div>
            </div>

            <div class="form-group row">
                <label for="phone" class="col-md-2 pr-3"><span class="font-weight-bold"><?php echo $this->lang->line('sales_phone');?></span><span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <input type="text" class="form-control" id="phone" name="phone" autocomplete="off" value="<?php echo set_value('name',$this->input->post('phone'));?>">
                </div>
            </div>

            <div class="form-group row">
                <label for="dni" class="col-md-2"><span class="font-weight-bold"><?php echo $this->lang->line('sales_dni');?></span><span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <input type="text" class="form-control" id="dni" name="dni" autocomplete="off" value="<?php echo set_value('name',$this->input->post('dni'));?>">
                </div>
            </div>

            <?php if ($data->campaign_extra_data && $data->campaign_extra_data->extra_data) { ?>
                <div class="form-group row">
                    <label for="census" class="col-md-2"><span class="font-weight-bold"><?php echo $this->lang->line('sales_census');?></span>
                        <?php if ($data->campaign_extra_data && $data->campaign_extra_data->extra_data_required) { ?>
                            <span class="text-danger"><strong> *</strong></span>
                        <?php }?>
                        </label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" id="census" name="census" autocomplete="off" value="<?php echo set_value('name',$this->input->post('census'));?>">
                    </div>
                </div>
            <?php }?>
            <?php if ($data->campaign_extra_data && $data->campaign_extra_data->extra_data) { ?>
                <div class="form-group row">
                    <label for="commercial_offer" class="col-md-2"><span class="font-weight-bold"><?php echo $this->lang->line('sales_commercial_offer');?></span>
                        <?php if ($data->campaign_extra_data && $data->campaign_extra_data->extra_data_required) { ?>
                            <span class="text-danger"><strong> *</strong></span>
                        <?php }?>
                    </label>
                    <div class="col-md-10">
                        <select class="form-control" id="commercial_offer" name="commercial_offer">
                            <option></option>
                            <option>Nivel 1</option>
                            <option>Nivel 2</option>
                            <option>Nivel 3</option>
                        </select>
                        </div>
                </div>
            <?php }?>
            <div id="errors">

            </div>

            <div class="row">
                <div class="col-md-12 text-center">
                    <button type="submit" id="submit" class="btn btn-success"><?php echo $this->lang->line('sales_create');?></button>
                    <span id="loading" class="d-none"><i class="fas fa-spinner fa-spin fa-2x"></i></span>
                </div>
            </div>
        </form>
        <div class="modal fade" id="no-complete" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-body">
                        <p><?php echo $this->lang->line('general_completeall')?> <strong class="text-danger">*</strong></p>
                        <div class="text-right">
                            <span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_accept'); ?></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } 
    else { ?>
        <div class="alert alert-danger">
            <strong><i class="fas fa-skull"></i></strong>
            <?php echo($this->lang->line('sales_not_permission')); ?>
        </div>
    <?php } ?>
</div>
</div>
<?php if($this->Identity_model->Validate('sales/admin')){ ?>
    <script>

        /*function searchProducts(campaignId) {
            selectProduct = document.getElementById('product');

            if(campaignId != "NULL"){

                var products = <?php echo $data->products; ?>;
                
                var findedProducts = [];
                
                for (let i = 0; i < products.length; i++) {
                    const product = products[i];
                    if(product.campaignId == campaignId){
                        findedProducts.push(product);
                    }
                }
                if(findedProducts.length > 0){
                    emptyCampaign = document.getElementById('emptyCampaign');

                    emptyCampaign.classList.add('d-none');
                    selectProduct.innerHTML ='<option value="NULL"></option>';
                    selectProduct.disabled = false;

                    findedProducts.forEach(element => {
                        selectProduct.innerHTML += `<option value=${element.productId}>${element.name}</option>`;
                    });
                }
                else{
                    selectProduct.innerHTML = "";
                    selectProduct.disabled = true;
                    emptyCampaign.classList.remove('d-none');
                }
            }
            else{
                selectProduct.innerHTML = "";
                selectProduct.disabled = true;
            }

            
        }*/

        function emptyForm(sellAgain) {
            $('#contractNumber').val('');
             //document.getElementById('campaign').selectedIndex = 0;
             //document.getElementById('product').innerHTML = "";
             //document.getElementById('product').disabled = true;
             document.getElementById('product').selectedIndex = 0;
             $('#comment').val('');

             if(!sellAgain)
             {   
                $('#phone').val('');
                $('#dni').val('');
            }

            enableForm();
        }

        function enableForm() {

            $('#saleMessage').addClass('d-none');
            $('.form-control').attr('disabled',false);
            $('#submit').attr('disabled',false);
        }

        $('#form').submit(function (e) {
            e.preventDefault();
            document.getElementById('errors').innerHTML = "";

            if($('#contractNumber').val() == "" || /*$('#campaign').val() == "NULL" ||*/ $('#product').val() == "NULL" || $('#phone').val() == "" || $('#dni').val() == "")
            {
                $('#no-complete').modal('show');
            }
            else 
            {
                $('#submit').hide();
                document.getElementById('loading').classList.remove('d-none');

                var formData = {
                    contractNumber  : $('#contractNumber').val(),
                   // campaign        : $('#campaign').val(),
                   product         : $('#product').val(),
                   comment         : $('#comment').val(),
                   phone           : $('#phone').val(),
                   dni             : $('#dni').val(),
                   census          : $('#census').val(),
                   commercial_offer: $('#commercial_offer').children("option:selected"). val(),
               }

               $.ajax({
                method 	    : 'POST',
                url 	    : 'createDb',
                data        : formData,
                beforeSend  : function () {
                    document.getElementById('saleMessage').classList.add('d-none');
                },
            }).done(function(data) {

                if(data == 'not_in_campaign')
                {
                    location.reload();
                }
                else
                {
                    $('#submit').show();
                    document.getElementById('loading').classList.add('d-none');
                    if(data == "success"){

                        document.getElementById('saleMessage').classList.remove('d-none');
                        document.getElementById('errors').classList.add('d-none');

                        $('.form-control').attr('disabled',true);
                        $('#submit').attr('disabled',true);
                    }
                    else{
                        document.getElementById('errors').classList.remove('d-none');
                        document.getElementById('errors').innerHTML = data;
                    }
                }

            });
        }
    })

</script>
<?php } ?>