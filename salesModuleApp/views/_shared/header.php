<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Sales Module</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap-->
    <link rel="stylesheet" href="/<?php echo ASSETSFOLDER; ?>/libraries/bootstrap-4.1.3-dist/css/bootstrap.min.css">
    <!-- Font Awesome --> 
    <link rel="stylesheet" href="/<?php echo ASSETSFOLDER; ?>/libraries/fontawesome-free-5.5.0/css/all.min.css">
    <!-- Datatables -->
    <link rel="stylesheet" href="/<?php echo ASSETSFOLDER; ?>/libraries/DataTables-1.10.18/css/jquery.dataTables.min.css">
    <!-- Datepicker -->
    <link rel="stylesheet" href="/<?php echo ASSETSFOLDER; ?>/libraries/bootstrap-datepicker/css/bootstrap-datepicker3.css">
    <!-- Custom styles -->
    <link rel="stylesheet" href="/<?php echo ASSETSFOLDER; ?>/css/custom-style.css">
    <link rel="icon" type="image/png" href="/<?php echo ASSETSFOLDER; ?>/images/favicon.png" />

    <!-- Jquery -->
    <script src="/<?php echo ASSETSFOLDER; ?>/libraries/jquery-3.3.1.min.js"></script>
    <!-- Bootstrap -->
    <script src="/<?php echo ASSETSFOLDER; ?>/libraries/bootstrap-4.1.3-dist/js/bootstrap.min.js"></script>
    <!-- Datatables -->
    <script src="/<?php echo ASSETSFOLDER; ?>/libraries/DataTables-1.10.18/js/jquery.dataTables.min.js"></script>

    <!--Datepicker-->
    <script src="/<?php echo ASSETSFOLDER; ?>/libraries/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="/<?php echo ASSETSFOLDER; ?>/libraries/bootstrap-datepicker/locales/bootstrap-datepicker.es.min.js"></script>
</head>
<body>
    <header class="navbar navbar-expand-lg navbar-dark bg-dark mb-4">
        <span class="navbar-brand">
            <img src="/<?php echo ASSETSFOLDER; ?>/images/logo-mini.png" alt="">
        </span>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <div class="navbar-nav-scroll">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/<?php echo FOLDERADD; ?>/sales/create"><?php echo($this->lang->line('general_sales')) ?></a>
                </li>
                <?php if($this->Identity_model->Validate('reports/viewall') || $this->Identity_model->Validate('reports/viewonly')){ ?>
                 <li class="nav-item">
                    <a class="nav-link" href="/<?php echo FOLDERADD; ?>/reports"><?php echo($this->lang->line('general_reports')) ?></a>
                </li>
            <?php } ?>
        </ul>
    </div>

    <ul class="navbar-nav ml-md-auto">
        <?php if($this->Identity_model->Validate('sales/admin') && isset($my_campaigns))
        { ?>
            <li class="nav-item mr-2 mt-1">
                <form method="POST" action="">
                    <select name="change_campaign_id" class="form-control" onchange="this.form.submit()">
                        <?php 
                        foreach ($my_campaigns as $data) { ?>
                            <option value="<?php echo $data->campaignId ?>" <?php echo set_select('change_campaign_id', $data->campaignId, $data->campaignId == $this->session->selectedCampaign); ?> ><?php echo  $data->name ?></option>
                        <?php   } ?>
                    </select>
                </form>
            </li>
            <?php
        } ?>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <?php echo $this->session->fullName?>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                <?php 
                if($this->Identity_model->Validate('users/changepass'))
                    { ?>
                        <a class="dropdown-item" href="/<?php echo FOLDERADD; ?>/users/changePass"><?php echo($this->lang->line('admin_users_changepass')) ?></a>
                        <div class="dropdown-divider"></div>
                        <?php
                    }
                    ?>
                    <a class="dropdown-item" href='/<?php echo FOLDERADD; ?>/login/logout'><i class="fas fa-sign-out-alt"></i> <?php echo($this->lang->line('main_log_out')) ?></a>
                </div>
            </li>
            <?php if ($this->Identity_model->Validate('users/view') || $this->Identity_model->Validate('users/admin') || $this->Identity_model->Validate('roles/view') || $this->Identity_model->Validate('roles/admin') || $this->Identity_model->Validate('teamleaders/view') || $this->Identity_model->Validate('teamleaders/admin') || $this->Identity_model->Validate('campaigns/view') || $this->Identity_model->Validate('campaigns/admin')) { ?>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="" id="dropConfig" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-cog"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                        <?php if ($this->Identity_model->Validate('users/view') || $this->Identity_model->Validate('users/admin')){ ?>
                            <a class="dropdown-item" href="/<?php echo FOLDERADD; ?>/users"><?php echo($this->lang->line('main_users')) ?></a>
                        <?php } 
                        if ($this->Identity_model->Validate('teamleaders/view') || $this->Identity_model->Validate('teamleaders/admin')){ ?>
                            <a class="dropdown-item" href="/<?php echo FOLDERADD; ?>/teamleaders"><?php echo($this->lang->line('main_teamleaders')) ?></a>
                        <?php }
                        if ($this->Identity_model->Validate('roles/view') || $this->Identity_model->Validate('roles/admin')){ ?>
                            <a class="dropdown-item" href="/<?php echo FOLDERADD; ?>/roles"><?php echo($this->lang->line('main_roles')) ?></a>
                        <?php } 
                        if ($this->Identity_model->Validate('hierarchy/view')){ ?>
                            <a class="dropdown-item" href="/<?php echo FOLDERADD; ?>/hierarchy"><?php echo($this->lang->line('main_hierarchy')) ?></a>
                        <?php }
                        if ($this->Identity_model->Validate('campaigns/view') || $this->Identity_model->Validate('campaigns/admin')){ ?>
                            <a class="dropdown-item" href="/<?php echo FOLDERADD; ?>/campaigns"><?php echo($this->lang->line('main_campaigns')) ?></a>
                        <?php } 
                        if ($this->Identity_model->Validate('users/reset')){ ?>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="/<?php echo FOLDERADD; ?>/users/resetPass"><?php echo($this->lang->line('admin_users_resetpass')) ?></a>
                        <?php } ?> 
                    </div>
                </li>
            <?php } ?>
        </ul>

    </div>
</header>
<div class="<?php echo((isset($max_width) && $max_width == TRUE) ? "ml-5 mr-5" : "container") ?>">
<script>

</script>
