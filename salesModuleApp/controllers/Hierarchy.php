<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hierarchy extends CI_Controller {


    public function __construct()
    {
        parent::__construct();
        $this->load->model('Hierarchy_model','Hierarchy');
    }
    

    public function index()
    {
        if($this->Identity_model->Validate('hierarchy/admin')){
            $hierarchy = $this->Hierarchy->GetHierarchys();
            $this->load->view('_shared/header');
            $this->load->view('hierarchy/index', array('hierarchy' => $hierarchy));
            $this->load->view('_shared/footer');
        }
        else {
            header('Location:/'.FOLDERADD);
        }
    }

    public function create()
    {
        if($this->Identity_model->Validate('hierarchy/admin')){
            $this->form_validation->set_rules('name', 'lang:general_name', 'required|max_length[50]|is_unique[hierarchy.name]');
            
            
            if ($this->form_validation->run() == FALSE) {
                $hierarchys = $this->Hierarchy->GetHierarchys();
                
                $this->load->view('_shared/header');
                $this->load->view('hierarchy/create',array('hierarchys' => $hierarchys));
                $this->load->view('_shared/footer');
            } 
            else {

                //busco la jerarquia mas baja y si el post es igual es porque la esta agregando al final de todo
                $sql ="SELECT hierarchyId FROM hierarchy ORDER BY hierarchyId DESC LIMIT 1";
                $lastId = $this->db->query($sql)->row();
                $hierarchyId = $this->input->post('hierarchy');

                if($hierarchyId == $lastId){
                    $hInsert = array('name' => $this->input->post('name',TRUE));
                    $res = $this->Hierarchy->Create($hInsert);
                }
                else{
                    $hierarchys = $this->Hierarchy->GetHierarchiesUnder($hierarchyId);
                    foreach ($hierarchys as $h) {
                        $hUpdate = array('hierarchyId' => $h->hierarchyId+1);
                        $this->Hierarchy->UpdateId($hUpdate,$h->hierarchyId);
                    }
                    $hInsert = array(
                        'hierarchyid'   => $hierarchyId+1,
                        'name'          => $this->input->post('name',TRUE),
                    );
                    $res = $this->Hierarchy->Create($hInsert);
                }

                if($res == "success"){
                    $this->session->set_flashdata('hierarchyMessage', 'create');
                    header('Location:/'.FOLDERADD.'/hierarchy'); 
                }
            }
            
        }
        else{
            header('Location:/'.FOLDERADD.'/hierarchy'); 
        }
    }

    public function editHierarchy()
    {
        if($this->Identity_model->Validate('hierarchy/admin')){
            
            $this->form_validation->set_rules('name', 'lang:general_name', 'required|max_length[50]|edit_unique[hierarchy.name.hierarchyId.'.$_POST['originalHierarchyId'].'.FALSE]');
            
            if(!isset($_POST['isSameLevel'])){

                $this->form_validation->set_rules('hierarchy', 'lang:admin_hierarchy_level', 'required|numeric|exist_hierarchy');
            }

            if ($this->form_validation->run() == FALSE)
            {
                $this->edit();
            }
            else
            {  
                $urlId = $this->uri->segment(3);
                $sql = "SELECT hierarchyId FROM hierarchy WHERE hierarchyId = ?";
                $response = $this->db->query($sql,$urlId)->result();
                if(isset($response))
                {
                    
                    $postHierarchyId = $this->input->post('hierarchy');
                    
                    if(isset($_POST['isSameLevel'])){
                        $hUpdate = array('name' => $this->input->post('name'));
                        $res = $this->Hierarchy->UpdateId($hUpdate,$urlId);               
                    }
                    else
                    {
                        if($urlId > $postHierarchyId){
                            $hUpdate = array(
                                'hierarchyId' => $postHierarchyId+1,
                                'name'        => $this->input->post('name')
                            );
                            $res = $this->Hierarchy->UpdateId($hUpdate,$urlId);

                            $hierarchys = $this->Hierarchy->GetHierarchiesUnderEdit($postHierarchyId,$urlId);
                            foreach ($hierarchys as $h) {
                                $hUpdate = array('hierarchyId' => $h->hierarchyId+1);
                                $this->Hierarchy->UpdateId($hUpdate,$h->hierarchyId);
                            }
                        }
                        else{
                            $hierarchys = $this->Hierarchy->GetHierarchiesUnderEdit($postHierarchyId,$urlId);
                            foreach ($hierarchys as $h) {
                                $hUpdate = array('hierarchyId' => $h->hierarchyId+1);
                                $this->Hierarchy->UpdateId($hUpdate,$h->hierarchyId);
                            }

                            $hUpdate = array(
                                'hierarchyId' => $postHierarchyId+1,
                                'name'        => $this->input->post('name')
                            );
                            $res = $this->Hierarchy->UpdateId($hUpdate,$urlId);
                        }

                    }

                    if($res == "success"){
                        $this->session->set_flashdata('hierarchyMessage', 'create');
                        header('Location:/'.FOLDERADD.'/hierarchy'); 
                    }
                }
            }
        }
        else{

            show_404();
        }
    }

    public function deleteHierarchy($value='')
    {
        if ($this->Identity_model->Validate('hierarchy/admin')) {

            if ($this->Hierarchy->hierarchyExists($this->input->post('hierarchyId'))){

                $isHierarchyAssigned = $this->Hierarchy->getHierarchyRoles($this->input->post('hierarchyId'));

                if (isset($isHierarchyAssigned) && sizeof($isHierarchyAssigned) > 0) {

                    $this->session->set_flashdata(array('rolesInUse' => $isHierarchyAssigned));
                    header('Location:/'.FOLDERADD.'/hierarchy/delete/'.$this->input->post('hierarchyId')); 
                }
                else{
                
                    $deleteSuccess = $this->Hierarchy->deleteHierarchy($this->input->post('hierarchyId'));

                    if ($deleteSuccess) {
                        
                        $this->session->set_flashdata('hierarchyMessage', 'delete');
                        header('Location:/'.FOLDERADD.'/hierarchy'); 
                    }
                    else{
                        header('Location:/'.FOLDERADD);
                    }
                }          
            }
            else{

                header('Location:/'.FOLDERADD.'/hierarchy'); 
            }
        }
        else{

            header('Location:/'.FOLDERADD); 
        }
    }

    //funciones para cargar vistas

    public function edit(){

        $url_id = $this->uri->segment(3);

        if($this->Identity_model->Validate('hierarchy/admin')){

            $this->_showHierarchyInfo($url_id,'edit');
        }
        else
        {
            header('Location:/'.FOLDERADD);
        }
    }

    public function delete(){

        $url_id = $this->uri->segment(3);

        if($this->Identity_model->Validate('hierarchy/admin')){

            $this->_showHierarchyInfo($url_id,'delete');
        }
        else
        {
            header('Location:/'.FOLDERADD);
        }
    }

    public function _showHierarchyInfo($hierarchyId, $type)
    {

        if (isset($hierarchyId) && is_numeric($hierarchyId) && $this->Hierarchy->hierarchyExists($hierarchyId)){

            $hierarchyData = $this->Hierarchy->GetHierarchy($hierarchyId);
            $hierarchyPosition = $this->Hierarchy->GetHierarchysEdit($hierarchyId);

            $isTopHierarchy = ($hierarchyId == 1)? true : false;

            $userHierarchy  = $this->Identity_model->GetHierarchy($this->session->UserId);
            $isHigherHierarchyLevel = ($hierarchyId > $userHierarchy)? true : false;

            $urlData = array('currentHierarchy' => $hierarchyData,'hierarchys' => $hierarchyPosition,'type' => $type,'topHierarchy' => $isTopHierarchy,'higherLevel'=> $isHigherHierarchyLevel );

            $this->load->view('_shared/header');
            $this->load->view('hierarchy/'.$type, $urlData);
            $this->load->view('_shared/footer');
        }
        else
        {
            header('Location:/'.FOLDERADD.'/hierarchy');
        }
        
    }

}
/* End of file Hierarchy.php */
?>
