<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Campaign_model');
	}

	public function index()
	{
		if ($this->session->has_userdata('Loged') && $this->session->has_userdata('UserId'))
		{
			header('Location:/'.FOLDERADD.'/sales/create');
		}
		else{
			$this->load->view('users/login');
		}
	}


	public function autenticate(){
		
		return $this->Identity_model->Autenticate();
	}

	public function logout()
	{
		$this->Identity_model->Logout();
	}
}
